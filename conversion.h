#include "stdafx.h"

	Bitmap^ iplImageToBitmap(IplImage *image);
	IplImage* convertImageToGreyscale(const IplImage *imageSrc);
	IplImage* resizeImage(const IplImage *origImg, int newWidth, int newHeight);
	IplImage* cropImage(const IplImage *img, const CvRect region);
	IplImage* convertFloatImageToUcharImage(const IplImage *srcImg);
	void saveFloatImage(const char *filename, const IplImage *srcImg);