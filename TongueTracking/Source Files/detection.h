#include "stdafx.h"

	CvRect detectFace(IplImage *inputImg, CvHaarClassifierCascade* cascade);
	IplImage* detectObjectsCustom(IplImage *img, cv::CascadeClassifier& cascade, CvScalar &col);