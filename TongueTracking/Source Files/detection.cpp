#include "stdafx.h"

	CvRect detectFace(IplImage *inputImg, CvHaarClassifierCascade* cascade)
		{

			// Smallest face size
			CvSize minFeatureSize = cvSize(20, 20);

			// Search for the biggest face.
			int flags = CV_HAAR_FIND_BIGGEST_OBJECT | CV_HAAR_DO_ROUGH_SEARCH;

			// Details of the search
			float search_scale_factor = 1.1f;
			IplImage *detectImg;
			IplImage *greyImg = 0;
			CvMemStorage* storage;
			CvRect rc;
			double t;
			CvSeq* rects;
			CvSize size;
			int nFaces;

			storage = cvCreateMemStorage(0);
			cvClearMemStorage( storage );


			// If the image is color, use a greyscale copy of the image.
			detectImg = (IplImage*)inputImg;
			if (inputImg->nChannels > 1) {
				size = cvSize(inputImg->width, inputImg->height);
				greyImg = cvCreateImage(size, IPL_DEPTH_8U, 1 );
				cvCvtColor( inputImg, greyImg, CV_BGR2GRAY );
				detectImg = greyImg;
			}

			// Detect all the faces in the greyscale image.
			t = (double)cvGetTickCount();
			rects = cvHaarDetectObjects( detectImg, cascade, storage, search_scale_factor, 3, flags, minFeatureSize);
			t = (double)cvGetTickCount() - t;
			nFaces = rects->total;

			cout << "[Detected " << nFaces << " faces in " << t
			<< " milliseconds]" << endl;

			// Get the first detected face (the biggest).
			if (nFaces > 0)
				rc = *(CvRect*)cvGetSeqElem( rects, 0 );
			else
				rc = cvRect(-1,-1,-1,-1);	// Couldn't find the face.

			if (greyImg)
				cvReleaseImage( &greyImg );
			cvReleaseMemStorage( &storage );
			//cvReleaseHaarClassifierCascade( &cascade );

			return rc;	// return the biggest face found, or (-1,-1,-1,-1).
		}

	IplImage* detectObjectsCustom(IplImage *img, cv::CascadeClassifier& cascade, CvScalar& col)
	{
		vector<cv::Rect> objects;
		float scale = 1.0f;
		int flags = CV_HAAR_SCALE_IMAGE;
		CvSize minFeatureSize = cvSize(20,20);
		float searchScaleFactor = 1.1f;
		int minNeighbors = 4;

		IplImage *gray = 0;	// Assume it's not used, so it's not freed.
		IplImage *smallImg = 0;	// Assume it's not used, so it's not freed.
		IplImage *inputImg = img;	// Use the original image by default.
		double t;
		t = (double)cvGetTickCount();	// record the timing.

		// If the input image is color, convert it from color to greyscale.
		if (img->nChannels >= 3)
		{
			gray = cvCreateImage( cvGetSize(img), 8, 1 );
			cvCvtColor( img, gray, CV_BGR2GRAY );
			inputImg = gray;	// use this new image.
		}

		// Possibly shrink the image, to run faster.
		if (scale < 0.9999f || scale > 1.0001f)
		{
			int smallWidth = cvRound(img->width/scale);
			int smallHeight = cvRound(img->height/scale);
			smallImg = cvCreateImage(cvSize(smallWidth, smallHeight), 8, 1);
			cvResize( inputImg, smallImg, CV_INTER_LINEAR );
			inputImg = smallImg;	// use this new image.
		}

		// Use Histogram Equalization to automatically standardize the
		// brightness and contrast, so that dark images look better.
		cvEqualizeHist( inputImg, inputImg );

		// Get a new OpenCV 2.0 C++ style image that
		// references the same IplImage of the C interface.
		cv::Mat img2(inputImg);

		//imshow("Mouth Region", img2);

		// Detect objects in the small greyscale image.
		cascade.detectMultiScale( img2, objects, searchScaleFactor,
		minNeighbors, flags, minFeatureSize );

		// Resize the results if the image was temporarily scaled smaller
		if (smallImg)
		{
		vector<cv::Rect>::iterator r;
			for (r = objects.begin(); r != objects.end(); r++ )
			{
				r->x = cvRound(r->x * scale);
				r->y = cvRound(r->y * scale);
				r->width = cvRound(r->width * scale);
				r->height = cvRound(r->height * scale);
			}
		}

		// Show the timing results.
		t = (double)cvGetTickCount() - t;
		int tms = cvRound(t/((double)cvGetTickFrequency()*1000.0));
		cout << "[Detected " << objects.size() << " mouth in " << tms
		<< " milliseconds]" << endl;


		if ( objects.size() > 0)
			cvRectangle(img, cvPoint(objects[0].x,objects[0].y) , cvPoint(objects[0].x+objects[0].width,objects[0].y+objects[0].height), col,2);

		// Free the C resources (but not the C++ resources).
		if (gray)
			cvReleaseImage(&gray);
		if (smallImg)
			cvReleaseImage(&smallImg);
	
		return img;
	}