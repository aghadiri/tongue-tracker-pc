#pragma once

namespace TongueTracking {

	using namespace std;
	using namespace System::Runtime::InteropServices;
	using namespace cv;

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Windows::Forms;
	using namespace System::Drawing;
	using namespace System::IO;
	using namespace System::Text;
	using namespace System::Runtime::InteropServices;

	/// <summary>
	/// Summary for UI
	/// </summary>
	public ref class UI : public System::Windows::Forms::Form
	{
	public:
		UI(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~UI()
		{
			if (components)
			{
				delete components;
			}
		}

		private: System::Void UI_Load(System::Object^  sender, System::EventArgs^  e)
        { 
            
        }
	private: System::Windows::Forms::Button^  CameraButton;

	private: System::Windows::Forms::PictureBox^  pictureBox1;


	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(UI::typeid));
			this->CameraButton = (gcnew System::Windows::Forms::Button());
			this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox1))->BeginInit();
			this->SuspendLayout();
			// 
			// CameraButton
			// 
			this->CameraButton->Location = System::Drawing::Point(677, 12);
			this->CameraButton->Name = L"CameraButton";
			this->CameraButton->Size = System::Drawing::Size(104, 42);
			this->CameraButton->TabIndex = 0;
			this->CameraButton->Text = L"Turn on Camera";
			this->CameraButton->UseVisualStyleBackColor = true;
			this->CameraButton->Click += gcnew System::EventHandler(this, &UI::CameraButton_Click);
			// 
			// pictureBox1
			// 
			this->pictureBox1->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"pictureBox1.Image")));
			this->pictureBox1->Location = System::Drawing::Point(12, 12);
			this->pictureBox1->Name = L"pictureBox1";
			this->pictureBox1->Size = System::Drawing::Size(640, 480);
			this->pictureBox1->TabIndex = 1;
			this->pictureBox1->TabStop = false;
			// 
			// UI
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(984, 504);
			this->Controls->Add(this->pictureBox1);
			this->Controls->Add(this->CameraButton);
			this->Name = L"UI";
			this->Text = L"UI";
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox1))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion
	
	// Haar Cascade files, used for face and mouth detection.
	//static const char *faceCascadeFilename = "haarcascade_frontalface_alt.xml";
	static const char *faceCascadeFilename = "haarcascade_frontalface_default.xml";
	//static const char *mouthCascadeFilename = "haarcascade_mcs_mouth.xml";

	// Convert Iplimage to Bitmap
	Bitmap^ iplImageToBitmap(IplImage *image)
    {
		Bitmap ^bitmap;
		if(image->nChannels == 1)
		{
			bitmap = gcnew Bitmap( image->width, image->height, System::Drawing::Imaging::PixelFormat::Format8bppIndexed);
 
			System::Drawing::Imaging::ColorPalette  ^palette = bitmap->Palette;
            for( int i=0; i<256; i++)
				palette->Entries[i] = Color::FromArgb(i,i,i);
			bitmap->Palette = palette;
		}

		else
			bitmap = gcnew Bitmap( image->width, image->height, System::Drawing::Imaging::PixelFormat::Format24bppRgb);
 
		System::Drawing::Imaging::BitmapData ^data = bitmap->LockBits(System::Drawing::Rectangle(0, 0, bitmap->Width, bitmap->Height), System::Drawing::Imaging::ImageLockMode::ReadWrite,  bitmap->PixelFormat);
		memcpy( data->Scan0.ToPointer(), image->imageData, image->imageSize );
		bitmap->UnlockBits( data );
 
		return bitmap;
    }


	private: System::Void CameraButton_Click(System::Object^  sender, System::EventArgs^  e) 
		{
			CvCapture* camera = 0;
			IplImage* frame = 0;
			IplImage* Img1 = 0;
			IplImage* Cloneframe = 0;
			IplImage* CloneframeROI = 0;
			int key = 0;
			CvRect faceRect;
			int FrameNumber = 0;

			if(CameraButton->Text == L"Turn on Camera")
			{
				//cvNamedWindow( "Camera", 1 );
				cvNamedWindow("Face", CV_WINDOW_AUTOSIZE); 

				// Create Camera
				if (!camera) 
				{
					camera = cvCreateCameraCapture( 0 );
					if (!camera) 
						MessageBox::Show("Couldn't access the camera." ,"Error");				 
				}

				// Set Camera Properties.
				/*cvSetCaptureProperty(camera, CV_CAP_PROP_FRAME_WIDTH, 320);
				cvSetCaptureProperty(camera, CV_CAP_PROP_FRAME_HEIGHT, 240);*/
				cvSetCaptureProperty(camera, CV_CAP_PROP_FRAME_WIDTH, 640);
				cvSetCaptureProperty(camera, CV_CAP_PROP_FRAME_HEIGHT, 480);

				//Sleep(1000);

				CameraButton->Text = L"Turn off Camera";

				CvHaarClassifierCascade *faceCascade;
				faceCascade = (CvHaarClassifierCascade*)cvLoad(faceCascadeFilename, 0, 0, 0);
				
				//const string face_cascade_name = "lbpcascade_frontalface.xml";
				//cv::CascadeClassifier face_cascade;
				//face_cascade.load( face_cascade_name );

				const string openmouth_cascade_name = "O1212.xml";
				cv::CascadeClassifier openmouth_cascade;
				openmouth_cascade.load( openmouth_cascade_name );
				
				const string closedmouth_cascade_name = "C1207.xml";
				cv::CascadeClassifier closedmouth_cascade;
				closedmouth_cascade.load( closedmouth_cascade_name );

				//IplImage* CloneframeROI;
				bool face=false;

				// Show camera in pictureBox.
				while( key != 1 ) 
				{	
					frame = cvQueryFrame( camera );					
					Cloneframe = cvCloneImage(frame);

					faceRect = detectFace(Cloneframe, faceCascade);
					faceRect.height = faceRect.height+30;
					cvRectangle(Cloneframe, cvPoint(faceRect.x,faceRect.y) , cvPoint(faceRect.x+faceRect.width,faceRect.y+faceRect.height), cvScalar(0, 255, 0),2);

					// Take training images
					//if (faceRect.x != -1)
					//{
					//	CloneframeROI = cvCloneImage(Cloneframe);
					//	System::String^ str = "1213-" + System::Convert::ToString(FrameNumber) + ".jpg";
					//	char* str2 = (char*)(void*)Marshal::StringToHGlobalAnsi(str);
					//	cvSetImageROI(CloneframeROI, cvRect(faceRect.x, faceRect.y, faceRect.width, faceRect.height));
					//	cvSaveImage(str2, CloneframeROI);
					//	FrameNumber++;
					//}

					if (faceRect.x != -1)
					{
						cvSetImageROI(Cloneframe, cvRect(faceRect.x, faceRect.y+(faceRect.height *1/2), faceRect.width, faceRect.height/2));

						Cloneframe = detectObjectsCustom(Cloneframe, openmouth_cascade, cvScalar(255, 255, 0));
						Cloneframe = detectObjectsCustom(Cloneframe, closedmouth_cascade, cvScalar(0, 255, 255));

						cvResetImageROI(Cloneframe);
					}
					
					cvShowImage("Face", Cloneframe);
					//Bitmap^ frame1 = iplImageToBitmap(Cloneframe);
					//pictureBox1->Image = frame1;
					if(CameraButton->Text == L"Turn on Camera")
					{	
						cvDestroyWindow("Camera");
						Img1 = cvLoadImage("webcam-icon.jpg", CV_LOAD_IMAGE_UNCHANGED);
						Bitmap^ Img2 = iplImageToBitmap(Img1);
						pictureBox1->Image = Img2;
						//cvReleaseCapture( &camera );
						break;
					}
				key = cvWaitKey( 1 );

				}
		
			} 

			else if(CameraButton->Text == L"Turn off Camera")
			{
				CameraButton->Text = L"Turn on Camera";

				cvReleaseImage( &frame );
				cvReleaseImage( &Cloneframe );
				cvReleaseImage( &Img1 );
				cvReleaseCapture( &camera );
				cvDestroyWindow("Face");
			}
		}
	
	};
}
