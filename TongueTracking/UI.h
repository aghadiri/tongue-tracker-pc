#pragma once

namespace TongueTracking {

	using namespace std;
	using namespace System::Runtime::InteropServices;
	using namespace cv;

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Windows::Forms;
	using namespace System::Drawing;
	using namespace System::IO;
	using namespace System::Text;
	using namespace System::Runtime::InteropServices;

	/// <summary>
	/// Summary for UI
	/// </summary>
	public ref class UI : public System::Windows::Forms::Form
	{
	public:
		UI(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~UI()
		{
			if (components)
			{
				delete components;
			}
		}

		private: System::Void UI_Load(System::Object^  sender, System::EventArgs^  e)
        { 
            
        }
	private: System::Windows::Forms::Button^  CameraButton;

	private: System::Windows::Forms::PictureBox^  pictureBox1;
	private: System::Windows::Forms::Button^  btnClosed;
	private: System::Windows::Forms::Button^  btnOpen;
	private: System::Windows::Forms::Button^  btnUp;
	private: System::Windows::Forms::Button^  btnDown;
	private: System::Windows::Forms::Button^  btnLeft;
	private: System::Windows::Forms::Button^  btnRight;
	private: System::Windows::Forms::Button^  btnTest;


	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(UI::typeid));
			this->CameraButton = (gcnew System::Windows::Forms::Button());
			this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
			this->btnClosed = (gcnew System::Windows::Forms::Button());
			this->btnOpen = (gcnew System::Windows::Forms::Button());
			this->btnUp = (gcnew System::Windows::Forms::Button());
			this->btnDown = (gcnew System::Windows::Forms::Button());
			this->btnLeft = (gcnew System::Windows::Forms::Button());
			this->btnRight = (gcnew System::Windows::Forms::Button());
			this->btnTest = (gcnew System::Windows::Forms::Button());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox1))->BeginInit();
			this->SuspendLayout();
			// 
			// CameraButton
			// 
			this->CameraButton->Location = System::Drawing::Point(46, 12);
			this->CameraButton->Name = L"CameraButton";
			this->CameraButton->Size = System::Drawing::Size(104, 42);
			this->CameraButton->TabIndex = 0;
			this->CameraButton->Text = L"Turn on Camera";
			this->CameraButton->UseVisualStyleBackColor = true;
			this->CameraButton->Click += gcnew System::EventHandler(this, &UI::CameraButton_Click);
			// 
			// pictureBox1
			// 
			this->pictureBox1->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"pictureBox1.Image")));
			this->pictureBox1->Location = System::Drawing::Point(12, 12);
			this->pictureBox1->Name = L"pictureBox1";
			this->pictureBox1->Size = System::Drawing::Size(12, 480);
			this->pictureBox1->TabIndex = 1;
			this->pictureBox1->TabStop = false;
			// 
			// btnClosed
			// 
			this->btnClosed->Location = System::Drawing::Point(201, 12);
			this->btnClosed->Name = L"btnClosed";
			this->btnClosed->Size = System::Drawing::Size(104, 42);
			this->btnClosed->TabIndex = 2;
			this->btnClosed->Text = L"Closed";
			this->btnClosed->UseVisualStyleBackColor = true;
			this->btnClosed->Click += gcnew System::EventHandler(this, &UI::btnClosed_Click);
			// 
			// btnOpen
			// 
			this->btnOpen->Location = System::Drawing::Point(201, 60);
			this->btnOpen->Name = L"btnOpen";
			this->btnOpen->Size = System::Drawing::Size(104, 42);
			this->btnOpen->TabIndex = 3;
			this->btnOpen->Text = L"Open";
			this->btnOpen->UseVisualStyleBackColor = true;
			this->btnOpen->Click += gcnew System::EventHandler(this, &UI::btnOpen_Click);
			// 
			// btnUp
			// 
			this->btnUp->Location = System::Drawing::Point(201, 252);
			this->btnUp->Name = L"btnUp";
			this->btnUp->Size = System::Drawing::Size(104, 42);
			this->btnUp->TabIndex = 4;
			this->btnUp->Text = L"Up";
			this->btnUp->UseVisualStyleBackColor = true;
			this->btnUp->Click += gcnew System::EventHandler(this, &UI::btnUp_Click);
			// 
			// btnDown
			// 
			this->btnDown->Location = System::Drawing::Point(201, 204);
			this->btnDown->Name = L"btnDown";
			this->btnDown->Size = System::Drawing::Size(104, 42);
			this->btnDown->TabIndex = 5;
			this->btnDown->Text = L"Down";
			this->btnDown->UseVisualStyleBackColor = true;
			this->btnDown->Click += gcnew System::EventHandler(this, &UI::btnDown_Click);
			// 
			// btnLeft
			// 
			this->btnLeft->Location = System::Drawing::Point(201, 156);
			this->btnLeft->Name = L"btnLeft";
			this->btnLeft->Size = System::Drawing::Size(104, 42);
			this->btnLeft->TabIndex = 6;
			this->btnLeft->Text = L"Left";
			this->btnLeft->UseVisualStyleBackColor = true;
			this->btnLeft->Click += gcnew System::EventHandler(this, &UI::btnLeft_Click);
			// 
			// btnRight
			// 
			this->btnRight->Location = System::Drawing::Point(201, 108);
			this->btnRight->Name = L"btnRight";
			this->btnRight->Size = System::Drawing::Size(104, 42);
			this->btnRight->TabIndex = 7;
			this->btnRight->Text = L"Right";
			this->btnRight->UseVisualStyleBackColor = true;
			this->btnRight->Click += gcnew System::EventHandler(this, &UI::btnRight_Click);
			// 
			// btnTest
			// 
			this->btnTest->Location = System::Drawing::Point(201, 359);
			this->btnTest->Name = L"btnTest";
			this->btnTest->Size = System::Drawing::Size(104, 42);
			this->btnTest->TabIndex = 8;
			this->btnTest->Text = L"Test";
			this->btnTest->UseVisualStyleBackColor = true;
			this->btnTest->Click += gcnew System::EventHandler(this, &UI::btnTest_Click);
			// 
			// UI
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(324, 504);
			this->Controls->Add(this->btnTest);
			this->Controls->Add(this->btnRight);
			this->Controls->Add(this->btnLeft);
			this->Controls->Add(this->btnDown);
			this->Controls->Add(this->btnUp);
			this->Controls->Add(this->btnOpen);
			this->Controls->Add(this->btnClosed);
			this->Controls->Add(this->pictureBox1);
			this->Controls->Add(this->CameraButton);
			this->Name = L"UI";
			this->Text = L"UI";
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox1))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion
	
	// Haar Cascade files, used for face and mouth detection.
	static const char *faceCascadeFilename = "haarcascade_frontalface_alt.xml";
	//static const char *faceCascadeFilename = "haarcascade_frontalface_default.xml";
	//static const char *mouthCascadeFilename = "haarcascade_mcs_mouth.xml";
	System::String^ mState;

	// Convert Iplimage to Bitmap
	Bitmap^ iplImageToBitmap(IplImage *image)
    {
		Bitmap ^bitmap;
		if(image->nChannels == 1)
		{
			bitmap = gcnew Bitmap( image->width, image->height, System::Drawing::Imaging::PixelFormat::Format8bppIndexed);
 
			System::Drawing::Imaging::ColorPalette  ^palette = bitmap->Palette;
            for( int i=0; i<256; i++)
				palette->Entries[i] = Color::FromArgb(i,i,i);
			bitmap->Palette = palette;
		}

		else
			bitmap = gcnew Bitmap( image->width, image->height, System::Drawing::Imaging::PixelFormat::Format24bppRgb);
 
		System::Drawing::Imaging::BitmapData ^data = bitmap->LockBits(System::Drawing::Rectangle(0, 0, bitmap->Width, bitmap->Height), System::Drawing::Imaging::ImageLockMode::ReadWrite,  bitmap->PixelFormat);
		memcpy( data->Scan0.ToPointer(), image->imageData, image->imageSize );
		bitmap->UnlockBits( data );
 
		return bitmap;
    }

	void cvOverlayImage(IplImage* src, IplImage* overlay, CvPoint location, CvScalar S, CvScalar D)
	{
	 int x,y,i;

	  for(x=0;x < overlay->width;x++)     
	//replace '-10' by whatever x position you want your overlay image to begin. 
	//say '-varX'
		{
			if(x+location.x>=src->width) continue;
			for(y=0;y < overlay->height;y++)  
	//replace '-10' by whatever y position you want your overlay image to begin.
	//say '-varY'
			{
				if(y+location.y>=src->height) continue;
				CvScalar source = cvGet2D(src, y+location.y, x+location.x);
				CvScalar over = cvGet2D(overlay, y, x);
				CvScalar merged;
				for(i=0;i<4;i++)
				merged.val[i] = (S.val[i]*source.val[i]+D.val[i]*over.val[i]);
				cvSet2D(src, y+location.y, x+location.x, merged);
			}
		}
	}


	private: System::Void CameraButton_Click(System::Object^  sender, System::EventArgs^  e) 
		{
			CvCapture* camera = 0;
			IplImage* frame = 0;
			IplImage* Img1 = 0;
			IplImage* Cloneframe = 0;
			IplImage* CloneframeROI = 0;
			IplImage* CloneframeT = 0;
			IplImage* up = 0;
			IplImage* down = 0;
			IplImage* right = 0;
			IplImage* left = 0;
			IplImage* open = 0;
			IplImage* closed = 0;
			int key = 0;
			CvRect faceRect;
			int FrameNumber = 0;
			//VideoWriter video("capture.avi",-1, 15, cvSize(640,480) );
			
			if(CameraButton->Text == L"Turn on Camera")
			{
				//cvNamedWindow( "Camera", 1 );
				cvNamedWindow("Face", CV_WINDOW_AUTOSIZE); 

				// Create Camera
				if (!camera) 
				{
					camera = cvCreateCameraCapture( 0 );
					if (!camera) 
						MessageBox::Show("Couldn't access the camera." ,"Error");				 
				}

				// Set Camera Properties.
				/*cvSetCaptureProperty(camera, CV_CAP_PROP_FRAME_WIDTH, 320);
				cvSetCaptureProperty(camera, CV_CAP_PROP_FRAME_HEIGHT, 240);*/
				cvSetCaptureProperty(camera, CV_CAP_PROP_FRAME_WIDTH, 640);
				cvSetCaptureProperty(camera, CV_CAP_PROP_FRAME_HEIGHT, 480);

				//Sleep(1000);

				CameraButton->Text = L"Turn off Camera";

				CvHaarClassifierCascade *faceCascade;
				faceCascade = (CvHaarClassifierCascade*)cvLoad(faceCascadeFilename, 0, 0, 0);
				
				//const string face_cascade_name = "lbpcascade_frontalface.xml";
				//cv::CascadeClassifier face_cascade;
				//face_cascade.load( face_cascade_name );

				// LBP Cascades, used for mouth detetction and toungue tracking.
				//const string openmouth_cascade_name = "O1214.xml";
				const string openmouth_cascade_name = "O0207P13456.xml";
				cv::CascadeClassifier openmouth_cascade;
				openmouth_cascade.load( openmouth_cascade_name );
				
				//const string closedmouth_cascade_name = "C1207.xml";
				const string closedmouth_cascade_name = "C0207P13456.xml";
				cv::CascadeClassifier closedmouth_cascade;
				closedmouth_cascade.load( closedmouth_cascade_name );

				//const string righttongue_cascade_name = "R1217.xml";
				const string righttongue_cascade_name = "R0208P13456.xml";
				cv::CascadeClassifier righttongue_cascade;
				righttongue_cascade.load( righttongue_cascade_name );

				//const string lefttongue_cascade_name = "L1219.xml";
				const string lefttongue_cascade_name = "L0208P13456.xml";
				cv::CascadeClassifier lefttongue_cascade;
				lefttongue_cascade.load( lefttongue_cascade_name );

				//const string downtongue_cascade_name = "D1219.xml";
				const string downtongue_cascade_name = "D0208P13456.xml";
				cv::CascadeClassifier downtongue_cascade;
				downtongue_cascade.load( downtongue_cascade_name );

				//const string uptongue_cascade_name = "U1220.xml";
				const string uptongue_cascade_name = "U0208P13456.xml";
				cv::CascadeClassifier uptongue_cascade;
				uptongue_cascade.load( uptongue_cascade_name );

				open = cvLoadImage("OPEN.jpg", CV_LOAD_IMAGE_COLOR);
				closed = cvLoadImage("CLOSED.jpg", CV_LOAD_IMAGE_COLOR);
				up = cvLoadImage("arrow-up.jpg", CV_LOAD_IMAGE_COLOR);
				down = cvLoadImage("arrow-down.jpg", CV_LOAD_IMAGE_COLOR);
				right = cvLoadImage("arrow-right.jpg", CV_LOAD_IMAGE_COLOR);
				left = cvLoadImage("arrow-left.jpg", CV_LOAD_IMAGE_COLOR);
				int frameN = 0;
				int rowSum = 0;
				const int NUM_OF_ROWS = 6;
				int frameStatus[NUM_OF_ROWS][6];
				int average[6];
				int x,y;
				int max = 0;
				int index = 1;
				int openStatus = 0;
				int closedStatus = 0;
				int rightStatus = 0;
				int leftStatus = 0;
				int downStatus = 0;
				int upStatus = 0;
				int colAve = 0;

				// Initialize frameStatus matrix to zeros.
				for(x = 0; x < NUM_OF_ROWS; x++)
				  for(y = 0; y < 6; y++) 
					  frameStatus[x][y] = 0;

				// Initialize average array to zeros.
				for(y = 0; y < 6; y++) 
					average[y] = 0;


				// Show camera in pictureBox.
				while( key != 1 ) 
				{	
					frame = cvQueryFrame( camera );					
					Cloneframe = cvCloneImage(frame);

					faceRect = detectFace(Cloneframe, faceCascade);
					faceRect.height = faceRect.height+40;
					cvRectangle(Cloneframe, cvPoint(faceRect.x,faceRect.y) , cvPoint(faceRect.x+faceRect.width,faceRect.y+faceRect.height), cvScalar(0, 255, 0),2);

					// Take training images

					if (faceRect.x != -1)
					{
						CloneframeROI = cvCloneImage(Cloneframe);
						System::String^ str = "Output\\P004-" + mState + System::Convert::ToString(FrameNumber) + ".jpg";
						char* str2 = (char*)(void*)Marshal::StringToHGlobalAnsi(str);
						cvSetImageROI(CloneframeROI, cvRect(faceRect.x, faceRect.y, faceRect.width, faceRect.height));
						cvSaveImage(str2, CloneframeROI);
						FrameNumber++;
					}

					// Testing 


					//if (faceRect.x != -1)
					//{
					//	for(y = 0; y < 6; y++)
					//		frameStatus[frameN][y] = 0;

					//	openStatus = 0;
					//	closedStatus = 0;
					//	rightStatus = 0;
					//	leftStatus = 0;
					//	downStatus = 0;
					//	upStatus = 0;

					//	cvSetImageROI(Cloneframe, cvRect(faceRect.x, faceRect.y+(faceRect.height *1/2), faceRect.width, faceRect.height/2));

					//	CloneframeT = detectObjectsCustom(Cloneframe, openmouth_cascade, 1);
					//	if (CloneframeT)
					//	{
					//		cvResetImageROI(Cloneframe);
					//		cvOverlayImage(Cloneframe, open, cvPoint(550, 300), cvScalar(0.5,0.5,0.5,0.5), cvScalar(0.5,0.5,0.5,0.5));
					//		cvSetImageROI(Cloneframe, cvRect(faceRect.x, faceRect.y+(faceRect.height *1/2), faceRect.width, faceRect.height/2));
					//		frameStatus[frameN][0] = 1;
					//		openStatus = 1;
					//	}

					//	CloneframeT = detectObjectsCustom(Cloneframe, closedmouth_cascade, 2);
					//	if (CloneframeT)
					//	{
					//		if(openStatus == 1)
					//			goto moreThanOne;
					//		else
					//		{
					//			cvResetImageROI(Cloneframe);
					//			cvOverlayImage(Cloneframe, closed, cvPoint(550, 300), cvScalar(0.5,0.5,0.5,0.5), cvScalar(0.5,0.5,0.5,0.5));
					//			cvSetImageROI(Cloneframe, cvRect(faceRect.x, faceRect.y+(faceRect.height *1/2), faceRect.width, faceRect.height/2));
					//			frameStatus[frameN][1] = 1;
					//			closedStatus = 1;
					//		}
					//	}


					//	CloneframeT = detectObjectsCustom(Cloneframe, righttongue_cascade, 0);
					//	if (CloneframeT)
					//	{
					//		if(openStatus == 1 || closedStatus == 1)
					//			goto moreThanOne;
					//		else
					//		{
					//			cvResetImageROI(Cloneframe);
					//			cvOverlayImage(Cloneframe, open, cvPoint(550, 300), cvScalar(0.5,0.5,0.5,0.5), cvScalar(0.5,0.5,0.5,0.5));
					//			cvOverlayImage(Cloneframe, left, cvPoint(550, 390), cvScalar(0.5,0.5,0.5,0.5), cvScalar(0.5,0.5,0.5,0.5));
					//			cvSetImageROI(Cloneframe, cvRect(faceRect.x, faceRect.y+(faceRect.height *1/2), faceRect.width, faceRect.height/2));
					//			frameStatus[frameN][2] = 1;
					//			rightStatus = 1;
					//		}
					//	}
					//	
					//	CloneframeT = detectObjectsCustom(Cloneframe, lefttongue_cascade, 0);
					//	if (CloneframeT)
					//	{
					//		if(openStatus == 1 || closedStatus == 1 || rightStatus == 1)
					//			goto moreThanOne;
					//		else
					//		{
					//			cvResetImageROI(Cloneframe);
					//			cvOverlayImage(Cloneframe, open, cvPoint(550, 300), cvScalar(0.5,0.5,0.5,0.5), cvScalar(0.5,0.5,0.5,0.5));
					//			cvOverlayImage(Cloneframe, right, cvPoint(550, 390), cvScalar(0.5,0.5,0.5,0.5), cvScalar(0.5,0.5,0.5,0.5));
					//			cvSetImageROI(Cloneframe, cvRect(faceRect.x, faceRect.y+(faceRect.height *1/2), faceRect.width, faceRect.height/2));
					//			frameStatus[frameN][3] = 1;
					//			leftStatus = 1;
					//		}
					//	}
					//	
					//	CloneframeT = detectObjectsCustom(Cloneframe, downtongue_cascade, 0);
					//	if (CloneframeT)
					//	{
					//		if(openStatus == 1 || closedStatus == 1 || rightStatus == 1 || leftStatus == 1)
					//			goto moreThanOne;
					//		else
					//		{
					//			cvResetImageROI(Cloneframe);
					//			cvOverlayImage(Cloneframe, open, cvPoint(550, 300), cvScalar(0.5,0.5,0.5,0.5), cvScalar(0.5,0.5,0.5,0.5));
					//			cvOverlayImage(Cloneframe, down, cvPoint(550, 390), cvScalar(0.5,0.5,0.5,0.5), cvScalar(0.5,0.5,0.5,0.5));
					//			cvSetImageROI(Cloneframe, cvRect(faceRect.x, faceRect.y+(faceRect.height *1/2), faceRect.width, faceRect.height/2));
					//			frameStatus[frameN][4] = 1;
					//			downStatus = 1;
					//		}
					//	}
					//	
					//	CloneframeT = detectObjectsCustom(Cloneframe, uptongue_cascade, 0);
					//	if (CloneframeT)
					//	{
					//		if(openStatus == 1 || closedStatus == 1 || rightStatus == 1 || leftStatus == 1 || downStatus == 1)
					//			goto moreThanOne;
					//		else
					//		{
					//			cvResetImageROI(Cloneframe);
					//			cvOverlayImage(Cloneframe, open, cvPoint(550, 300), cvScalar(0.5,0.5,0.5,0.5), cvScalar(0.5,0.5,0.5,0.5));
					//			cvOverlayImage(Cloneframe, up, cvPoint(550, 390), cvScalar(0.5,0.5,0.5,0.5), cvScalar(0.5,0.5,0.5,0.5));
					//			cvSetImageROI(Cloneframe, cvRect(faceRect.x, faceRect.y+(faceRect.height *1/2), faceRect.width, faceRect.height/2));
					//			frameStatus[frameN][5] = 1;
					//			upStatus = 1;
					//		}
					//	}
					//	
					//	goto done;


					//	moreThanOne:
					//	// See if no cascade has detected anything or more than one casacade detected.

					//	for(y = 0; y < 6; y++) 
					//		average[y] = 0;

					//		// Take the average of the output of each casacde.
					//		for(y = 0; y < 6; y++)
					//		{	
					//			colAve = 0;
					//			for(x = 0; x < NUM_OF_ROWS; x++)
					//			{
					//				colAve += frameStatus[x][y];
					//			}
					//			average[y] = colAve; 
					//		}

					//		// Find the maximum in average array (the most likely state).
					//		max = 0;
					//		index = 1;
					//		for(y = 0; y < 6; y++)
					//		{
					//			if(average[y] > max)
					//			{
					//				max = average[y];
					//				index = y;
					//			}
					//		}

					//		if(index == 0)
					//		{
					//			cvResetImageROI(Cloneframe);
					//			cvOverlayImage(Cloneframe, open, cvPoint(550, 300), cvScalar(0.5,0.5,0.5,0.5), cvScalar(0.5,0.5,0.5,0.5));
					//			cvSetImageROI(Cloneframe, cvRect(faceRect.x, faceRect.y+(faceRect.height *1/2), faceRect.width, faceRect.height/2));
					//			frameStatus[frameN][0] = 1;
					//		}

					//		if(index == 1)
					//		{
					//			cvResetImageROI(Cloneframe);
					//			cvOverlayImage(Cloneframe, closed, cvPoint(550, 300), cvScalar(0.5,0.5,0.5,0.5), cvScalar(0.5,0.5,0.5,0.5));
					//			cvSetImageROI(Cloneframe, cvRect(faceRect.x, faceRect.y+(faceRect.height *1/2), faceRect.width, faceRect.height/2));
					//			frameStatus[frameN][1] = 1;
					//		}

					//		if(index == 2)
					//		{
					//			cvResetImageROI(Cloneframe);
					//			cvOverlayImage(Cloneframe, open, cvPoint(550, 300), cvScalar(0.5,0.5,0.5,0.5), cvScalar(0.5,0.5,0.5,0.5));
					//			cvOverlayImage(Cloneframe, left, cvPoint(550, 390), cvScalar(0.5,0.5,0.5,0.5), cvScalar(0.5,0.5,0.5,0.5));
					//			cvSetImageROI(Cloneframe, cvRect(faceRect.x, faceRect.y+(faceRect.height *1/2), faceRect.width, faceRect.height/2));
					//			frameStatus[frameN][2] = 1;
					//		}

					//		if(index == 3)
					//		{	
					//			cvResetImageROI(Cloneframe);
					//			cvOverlayImage(Cloneframe, open, cvPoint(550, 300), cvScalar(0.5,0.5,0.5,0.5), cvScalar(0.5,0.5,0.5,0.5));
					//			cvOverlayImage(Cloneframe, right, cvPoint(550, 390), cvScalar(0.5,0.5,0.5,0.5), cvScalar(0.5,0.5,0.5,0.5));
					//			cvSetImageROI(Cloneframe, cvRect(faceRect.x, faceRect.y+(faceRect.height *1/2), faceRect.width, faceRect.height/2));
					//			frameStatus[frameN][3] = 1;
					//		}

					//		if(index == 4)
					//		{	
					//			cvResetImageROI(Cloneframe);
					//			cvOverlayImage(Cloneframe, open, cvPoint(550, 300), cvScalar(0.5,0.5,0.5,0.5), cvScalar(0.5,0.5,0.5,0.5));
					//			cvOverlayImage(Cloneframe, down, cvPoint(550, 390), cvScalar(0.5,0.5,0.5,0.5), cvScalar(0.5,0.5,0.5,0.5));
					//			cvSetImageROI(Cloneframe, cvRect(faceRect.x, faceRect.y+(faceRect.height *1/2), faceRect.width, faceRect.height/2));
					//			frameStatus[frameN][4] = 1;
					//		}

					//		if(index == 5)
					//		{		
					//			cvResetImageROI(Cloneframe);
					//			cvOverlayImage(Cloneframe, open, cvPoint(550, 300), cvScalar(0.5,0.5,0.5,0.5), cvScalar(0.5,0.5,0.5,0.5));
					//			cvOverlayImage(Cloneframe, up, cvPoint(550, 390), cvScalar(0.5,0.5,0.5,0.5), cvScalar(0.5,0.5,0.5,0.5));
					//			cvSetImageROI(Cloneframe, cvRect(faceRect.x, faceRect.y+(faceRect.height *1/2), faceRect.width, faceRect.height/2));
					//			frameStatus[frameN][5] = 1;
					//		}

					//		printf("Averages: %d,%d,%d,%d,%d,%d\n", average[0], average[1], average[2], average[3], average[4], average[5]);
					//		printf("index: %d\n", index);
					//			
					//		done:
					//		printf("Current Frame number: %d  Status: %d,%d,%d,%d,%d,%d\n", frameN, frameStatus[frameN][0], frameStatus[frameN][1], frameStatus[frameN][2], frameStatus[frameN][3], frameStatus[frameN][4], frameStatus[frameN][5]);
					//		
					//		//printf("Current frame number: %d", frameN);

					//		for (x = 0; x < NUM_OF_ROWS; x++)
					//		{
					//			printf("Frame number: %d  Status: %d,%d,%d,%d,%d,%d\n", x, frameStatus[x][0], frameStatus[x][1], frameStatus[x][2], frameStatus[x][3], frameStatus[x][4], frameStatus[x][5]);
					//		}
					//		

					//		cvResetImageROI(Cloneframe);

					//	
					//			
					//}  // end of if face is detected.
					
					cvShowImage("Face", Cloneframe);

					frameN++;

					if(frameN >= NUM_OF_ROWS)
						frameN = 0;

					//FrameNumber++;
					//if (FrameNumber>=20)
					//{
					//	cv::Mat frame(Cloneframe);
					//	video << frame;
					//}

					//Bitmap^ frame1 = iplImageToBitmap(Cloneframe);
					//pictureBox1->Image = frame1;
					if(CameraButton->Text == L"Turn on Camera")
					{	
						cvDestroyWindow("Camera");
						/*Img1 = cvLoadImage("webcam-icon.jpg", CV_LOAD_IMAGE_UNCHANGED);
						Bitmap^ Img2 = iplImageToBitmap(Img1);
						pictureBox1->Image = Img2;*/
						cvReleaseCapture( &camera );
						break;
					}
				key = cvWaitKey( 1 );

				} // end of while
			} 

			else if(CameraButton->Text == L"Turn off Camera")
			{
				CameraButton->Text = L"Turn on Camera";

				cvReleaseCapture( &camera );
				cvReleaseImage( &open );
				cvReleaseImage( &closed );
				cvReleaseImage( &right );
				cvReleaseImage( &left );
				cvReleaseImage( &up );
				cvReleaseImage( &down );
				cvReleaseImage( &frame );
				cvReleaseImage( &Cloneframe );
				cvReleaseImage( &CloneframeROI );
				cvReleaseImage( &CloneframeT );
				cvReleaseImage( &Img1 );
				cvDestroyWindow("Face");

				//cvReleaseVideoWriter( &writer );
			}
		}
	
	private: System::Void btnClosed_Click(System::Object^  sender, System::EventArgs^  e) {
				 mState = "Closed";
			 }
	private: System::Void btnOpen_Click(System::Object^  sender, System::EventArgs^  e) {
				 mState = "Open";
			 }
	private: System::Void btnRight_Click(System::Object^  sender, System::EventArgs^  e) {
				 mState = "Right";
			 }
	private: System::Void btnLeft_Click(System::Object^  sender, System::EventArgs^  e) {
				 mState = "Left";
			 }
	private: System::Void btnDown_Click(System::Object^  sender, System::EventArgs^  e) {
				 mState = "Down";
			 }
	private: System::Void btnUp_Click(System::Object^  sender, System::EventArgs^  e) {
				 mState = "Up";
			 }
	private: System::Void btnTest_Click(System::Object^  sender, System::EventArgs^  e) {

				IplImage* frame = 0;
				IplImage* Img1 = 0;
				IplImage* Cloneframe = 0;
				IplImage* CloneframeROI = 0;
				IplImage* CloneframeT = 0;
				IplImage* closed = 0;
				int key = 0;
				CvRect faceRect;
				int FrameNumber = 0;

				CvHaarClassifierCascade *faceCascade;
				faceCascade = (CvHaarClassifierCascade*)cvLoad(faceCascadeFilename, 0, 0, 0);

				const string closedmouth_cascade_name = "L23456.xml";
				cv::CascadeClassifier closedmouth_cascade;
				closedmouth_cascade.load( closedmouth_cascade_name );

				closed = cvLoadImage("arrow-right.jpg", CV_LOAD_IMAGE_UNCHANGED);

				 for(int i=1; i<=1131; i++)
				 {
					System::String^ str = "Test\\Test(" + i + ").jpg";
					char* str2 = (char*)(void*)Marshal::StringToHGlobalAnsi(str);

					Cloneframe = cvLoadImage(str2, CV_LOAD_IMAGE_UNCHANGED);

					faceRect = detectFace(Cloneframe, faceCascade);
					faceRect.height = faceRect.height+40;
					cvRectangle(Cloneframe, cvPoint(faceRect.x,faceRect.y) , cvPoint(faceRect.x+faceRect.width,faceRect.y+faceRect.height), cvScalar(0, 255, 0),2);

					if( faceRect.x !=-1)
					{
					cvSetImageROI(Cloneframe, cvRect(faceRect.x, faceRect.y+(faceRect.height *1/2), faceRect.width, faceRect.height/2));
					CloneframeT = detectObjectsCustom(Cloneframe, closedmouth_cascade, 2);
						if (CloneframeT)
						{
							cvResetImageROI(Cloneframe);
							//cvOverlayImage(Cloneframe, closed, cvPoint(faceRect.width/2, faceRect.height/2), cvScalar(0.5,0.5,0.5,0.5), cvScalar(0.5,0.5,0.5,0.5));
							//cvSetImageROI(Cloneframe, cvRect(faceRect.x, faceRect.y+(faceRect.height *1/2), faceRect.width, faceRect.height/2));
							
							cvSaveImage(str2, Cloneframe);
							System::String^ str5 = "Test\\Detected(" + i + ").jpg";
							char* str6 = (char*)(void*)Marshal::StringToHGlobalAnsi(str5);
							rename(str2, str6);
						}
						else
						{
							cvSaveImage(str2, Cloneframe);
							System::String^ str5 = "Test\\NoMouth(" + i + ").jpg";
							char* str6 = (char*)(void*)Marshal::StringToHGlobalAnsi(str5);
							rename(str2, str6);
						}

					}

					else
						cvSaveImage(str2, Cloneframe);
				 }
			 }
};
}
