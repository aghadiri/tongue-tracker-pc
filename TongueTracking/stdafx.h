// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
#pragma once

#include <iostream>
#include <stdio.h>

#include <cv.h>	
#include <cvaux.h>
#include <cxcore.h>
#include <highgui.h>

#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include "detection.h"
//#include "conversion.h"

	using namespace std;
	using namespace System::Runtime::InteropServices;
	using namespace cv;

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Windows::Forms;
	using namespace System::Drawing;
	using namespace System::IO;
	using namespace System::Text;
	using namespace System::Runtime::InteropServices;

// TODO: reference additional headers your program requires here
