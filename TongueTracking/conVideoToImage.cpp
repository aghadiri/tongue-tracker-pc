CvHaarClassifierCascade *faceCascade;
faceCascade = (CvHaarClassifierCascade*)cvLoad(faceCascadeFilename, 0, 0, 0);
VideoCapture video1 = VideoCapture("capture.avi");
cv::Mat image1;
int frameN=0;
if(video1.isOpened())
{
 while(video1.grab())
 {
	 video1.retrieve(image1,0);
	 System::String^ str = frameN + ".jpg";
	 char* str2 = (char*)(void*)Marshal::StringToHGlobalAnsi(str);
	 //imwrite(str2, image1);
	 IplImage* Cloneframe=cvCloneImage(&(IplImage)image1);
	 CvRect faceRect;
	 faceRect = detectFace(Cloneframe, faceCascade);
	 faceRect.height = faceRect.height+40;
	 cvSetImageROI(Cloneframe, cvRect(faceRect.x, faceRect.y, faceRect.width, faceRect.height));
	 cvSaveImage(str2, Cloneframe);
	 frameN++;
 }
}