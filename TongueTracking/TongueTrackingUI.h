#include "detection.h"
#include "conversion.h"

#pragma once

namespace FaceRecognizer {
	
	using namespace std;
	using namespace System::Runtime::InteropServices;
	using namespace cv;

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Windows::Forms;
	using namespace System::Drawing;
	using namespace System::IO;
	using namespace System::Text;
	using namespace System::Runtime::InteropServices;
		

	/// <summary>
	/// Summary for TongueTrackingUI
	/// </summary>
	public ref class TongueTrackingUI : public System::Windows::Forms::Form
	{
	public:
		TongueTrackingUI(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~TongueTrackingUI()
		{
			if (components)
			{
				delete components;
			}
		}
	
		private: System::Void TongueTrackingUI_Load(System::Object^  sender, System::EventArgs^  e)
        { 
            
        }

	private: System::Windows::Forms::Button^  CameraButton;
	private: System::Windows::Forms::Button^  StartMouthTrackingButton;
	private: System::Windows::Forms::PictureBox^  pictureBox1;
	private: System::ComponentModel::IContainer^  components;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(TongueTrackingUI::typeid));
			this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
			this->CameraButton = (gcnew System::Windows::Forms::Button());
			this->StartMouthTrackingButton = (gcnew System::Windows::Forms::Button());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox1))->BeginInit();
			this->SuspendLayout();
			// 
			// pictureBox1
			// 
			this->pictureBox1->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"pictureBox1.Image")));
			this->pictureBox1->Location = System::Drawing::Point(12, 13);
			this->pictureBox1->Name = L"pictureBox1";
			this->pictureBox1->Size = System::Drawing::Size(640, 480);
			this->pictureBox1->TabIndex = 3;
			this->pictureBox1->TabStop = false;
			// 
			// CameraButton
			// 
			this->CameraButton->Location = System::Drawing::Point(687, 13);
			this->CameraButton->Name = L"CameraButton";
			this->CameraButton->Size = System::Drawing::Size(112, 38);
			this->CameraButton->TabIndex = 4;
			this->CameraButton->Text = L"Turn on Camera";
			this->CameraButton->UseVisualStyleBackColor = true;
			this->CameraButton->Click += gcnew System::EventHandler(this, &TongueTrackingUI::CameraButton_Click);
			// 
			// StartMouthTrackingButton
			// 
			this->StartMouthTrackingButton->Location = System::Drawing::Point(687, 67);
			this->StartMouthTrackingButton->Name = L"StartMouthTrackingButton";
			this->StartMouthTrackingButton->Size = System::Drawing::Size(112, 38);
			this->StartMouthTrackingButton->TabIndex = 9;
			this->StartMouthTrackingButton->Text = L"Start Mouth Tracking";
			this->StartMouthTrackingButton->UseVisualStyleBackColor = true;
			this->StartMouthTrackingButton->Click += gcnew System::EventHandler(this, &TongueTrackingUI::StartMouthTrackingButton_Click);
			// 
			// TongueTrackingUI
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(916, 505);
			this->Controls->Add(this->StartMouthTrackingButton);
			this->Controls->Add(this->CameraButton);
			this->Controls->Add(this->pictureBox1);
			this->Name = L"TongueTrackingUI";
			this->Text = L"Face Recognizer";
			this->Load += gcnew System::EventHandler(this, &TongueTrackingUI::TongueTrackingUI_Load);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox1))->EndInit();
			this->ResumeLayout(false);

		}
		
#pragma endregion

	// Haar Cascade files, used for face and mouth detection.
	//static const char *faceCascadeFilename = "haarcascade_frontalface_alt.xml";
	static const char *faceCascadeFilename = "haarcascade_frontalface_default.xml";

	//static const char *mouthCascadeFilename = "haarcascade_mcs_mouth.xml";

	// Global Variables
	static CvCapture * camera			 = 0;	// Camera
	static int FrameNumber = 0;

	// "StartMouthTrackingButton" button
	private: System::Void StartMouthTrackingButton_Click(System::Object^  sender, System::EventArgs^  e)
	{

	}
	
	
	// "Turn On/Off Camera" button
	private: System::Void CameraButton_Click(System::Object ^ sender, System::EventArgs ^ e)
		{
			IplImage* frame;
			IplImage* Img1;
			int key=0;

			if(CameraButton->Text == L"Turn on Camera")
			{
				cvNamedWindow( "Camera", 1 );

				// Create Camera
				if (!camera) 
				{
					camera = cvCreateCameraCapture( 0 );
					if (!camera) 
						MessageBox::Show("Couldn't access the camera." ,"Error");				 
				}

				// Set Camera Properties.
				/*cvSetCaptureProperty(camera, CV_CAP_PROP_FRAME_WIDTH, 320);
				cvSetCaptureProperty(camera, CV_CAP_PROP_FRAME_HEIGHT, 240);*/
				cvSetCaptureProperty(camera, CV_CAP_PROP_FRAME_WIDTH, 640);
				cvSetCaptureProperty(camera, CV_CAP_PROP_FRAME_HEIGHT, 480);

				Sleep(1000);

				CameraButton->Text = L"Turn off Camera";

				CvRect faceRect;
			
				CvHaarClassifierCascade *faceCascade;
				faceCascade = (CvHaarClassifierCascade*)cvLoad(faceCascadeFilename, 0, 0, 0);
				
				const string face_cascade_name = "lbpcascade_frontalface.xml";
				cv::CascadeClassifier face_cascade;
				face_cascade.load( face_cascade_name );

				const string openmouth_cascade_name = "openmouth.xml";
				cv::CascadeClassifier openmouth_cascade;
				openmouth_cascade.load( openmouth_cascade_name );
				
				const string closedmouth_cascade_name = "C1207.xml";
				cv::CascadeClassifier closedmouth_cascade;
				closedmouth_cascade.load( closedmouth_cascade_name );

				IplImage* Cloneframe;
				IplImage* CloneframeROI;
				bool face=false;

				// Show camera in pictureBox.
				while( key != 1 ) 
				{	

					frame = cvQueryFrame( camera );					
					Cloneframe = cvCloneImage(frame);
					CloneframeROI = cvCloneImage(Cloneframe);

					faceRect = detectFace(Cloneframe, faceCascade);
					faceRect.height = faceRect.height+50;
					cvRectangle(Cloneframe, cvPoint(faceRect.x,faceRect.y) , cvPoint(faceRect.x+faceRect.width,faceRect.y+faceRect.height), cvScalar(0, 255, 0),2);

					// Take training images
					if (faceRect.x != -1)
					{
						/*System::String^ str = "1210-" + System::Convert::ToString(FrameNumber) + ".jpg";
						char* str2 = (char*)(void*)Marshal::StringToHGlobalAnsi(str);
						cvSetImageROI(CloneframeROI, cvRect(faceRect.x, faceRect.y, faceRect.width, faceRect.height));
						cvSaveImage(str2, CloneframeROI);
						FrameNumber++;*/
					}
/*
					if (faceRect.x != -1)
					{
						face=true;

						cvSetImageROI(Cloneframe, cvRect(faceRect.x, faceRect.y+(faceRect.height *1/2), faceRect.width, faceRect.height/2));

						Cloneframe = detectObjectsCustom(Cloneframe, openmouth_cascade, cvScalar(255, 255, 0));
						Cloneframe = detectObjectsCustom(Cloneframe, closedmouth_cascade, cvScalar(0, 255, 255));
					}

					if (face)
						cvResetImageROI(Cloneframe);
*/
					cvShowImage("Face", Cloneframe);
					//Bitmap^ frame1 = iplImageToBitmap(Cloneframe);
					//pictureBox1->Image = frame1;
					if(CameraButton->Text == L"Turn on Camera")
					{	
						cvDestroyWindow("Camera");
						Img1 = cvLoadImage("webcam-icon.jpg", CV_LOAD_IMAGE_UNCHANGED);
						Bitmap^ Img2 = iplImageToBitmap(Img1);
						pictureBox1->Image = Img2;
						//cvReleaseCapture( &camera );
						break;
					}
				key = cvWaitKey( 1 );

				}
		
			} 

			else if(CameraButton->Text == L"Turn off Camera")
			{
				CameraButton->Text = L"Turn on Camera";
			
			}
		}	

};
}

