#include "stdafx.h"

	// Convert Iplimage to Bitmap
	Bitmap^ iplImageToBitmap(IplImage *image)
    {
		Bitmap ^bitmap;
		if(image->nChannels == 1)
		{
			bitmap = gcnew Bitmap( image->width, image->height, System::Drawing::Imaging::PixelFormat::Format8bppIndexed);
 
			System::Drawing::Imaging::ColorPalette  ^palette = bitmap->Palette;
            for( int i=0; i<256; i++)
				palette->Entries[i] = Color::FromArgb(i,i,i);
			bitmap->Palette = palette;
		}

		else
			bitmap = gcnew Bitmap( image->width, image->height, System::Drawing::Imaging::PixelFormat::Format24bppRgb);
 
		System::Drawing::Imaging::BitmapData ^data = bitmap->LockBits(System::Drawing::Rectangle(0, 0, bitmap->Width, bitmap->Height), System::Drawing::Imaging::ImageLockMode::ReadWrite,  bitmap->PixelFormat);
		memcpy( data->Scan0.ToPointer(), image->imageData, image->imageSize );
		bitmap->UnlockBits( data );
 
		return bitmap;
    }

	//Return a new image that is always greyscale, whether the input image was RGB or Greyscale.
	//Remember to free the returned image using cvReleaseImage() when finished.
	IplImage* convertImageToGreyscale(const IplImage *imageSrc)
	{
		IplImage *imageGrey;
		// Either convert the image to greyscale, or make a copy of the existing greyscale image.
		// This is to make sure that the user can always call cvReleaseImage() on the output, whether it was greyscale or not.
		if (imageSrc->nChannels == 3) {
			imageGrey = cvCreateImage( cvGetSize(imageSrc), IPL_DEPTH_8U, 1 );
			cvCvtColor( imageSrc, imageGrey, CV_BGR2GRAY );
		}
		else {
			imageGrey = cvCloneImage(imageSrc);
		}
		return imageGrey;
	}

	// Creates a new image copy that is of a desired size.
	// Remember to free the new image later.
	IplImage* resizeImage(const IplImage *origImg, int newWidth, int newHeight)
	{
		IplImage *outImg = 0;
		int origWidth;
		int origHeight;
		if (origImg) {
			origWidth = origImg->width;
			origHeight = origImg->height;
		}
		if (newWidth <= 0 || newHeight <= 0 || origImg == 0 || origWidth <= 0 || origHeight <= 0) 
			MessageBox::Show("ERROR in resizeImage: Bad desired image size of " + newWidth + newHeight );

		// Scale the image to the new dimensions, even if the aspect ratio will be changed.
		outImg = cvCreateImage(cvSize(newWidth, newHeight), origImg->depth, origImg->nChannels);
		if (newWidth > origImg->width && newHeight > origImg->height) {
			// Make the image larger
			cvResetImageROI((IplImage*)origImg);
			cvResize(origImg, outImg, CV_INTER_LINEAR);	// CV_INTER_CUBIC or CV_INTER_LINEAR is good for enlarging
		}
		else {
			// Make the image smaller
			cvResetImageROI((IplImage*)origImg);
			cvResize(origImg, outImg, CV_INTER_AREA);	// CV_INTER_AREA is good for shrinking / decimation, but bad at enlarging.
		}

		return outImg;
	}

	// Returns a new image that is a cropped version of the original image. 
	IplImage* cropImage(const IplImage *img, const CvRect region)
	{
		IplImage *imageTmp;
		IplImage *imageRGB;
		CvSize size;
		size.height = img->height;
		size.width = img->width;

		if (img->depth != IPL_DEPTH_8U) 
			MessageBox::Show("ERROR in cropImage: Unknown image depth of %d given in cropImage() instead of 8 bits per pixel.");

		// First create a new (color or greyscale) IPL Image and copy contents of img into it.
		imageTmp = cvCreateImage(size, IPL_DEPTH_8U, img->nChannels);
		cvCopy(img, imageTmp, NULL);

		// Create a new image of the detected region Set region of interest to that surrounding the face
		cvSetImageROI(imageTmp, region);
		// Copy region of interest (i.e. face) into a new iplImage (imageRGB) and return it
		size.width = region.width;
		size.height = region.height;
		imageRGB = cvCreateImage(size, IPL_DEPTH_8U, img->nChannels);
		cvCopy(imageTmp, imageRGB, NULL);	// Copy just the region.

		cvReleaseImage( &imageTmp );
		return imageRGB;		
	}

	// Get an 8-bit equivalent of the 32-bit Float image.
	// Returns a new image, so remember to call 'cvReleaseImage()' on the result.
	IplImage* convertFloatImageToUcharImage(const IplImage *srcImg)
	{
		IplImage *dstImg = 0;
		if ((srcImg) && (srcImg->width > 0 && srcImg->height > 0)) {

			// Spread the 32bit floating point pixels to fit within 8bit pixel range.
			double minVal, maxVal;
			cvMinMaxLoc(srcImg, &minVal, &maxVal);

			// Deal with NaN and extreme values, since the DFT seems to give some NaN results.
			if (cvIsNaN(minVal) || minVal < -1e30)
				minVal = -1e30;
			if (cvIsNaN(maxVal) || maxVal > 1e30)
				maxVal = 1e30;
			if (maxVal-minVal == 0.0f)
				maxVal = minVal + 0.001;	// remove potential divide by zero errors.

			// Convert the format
			dstImg = cvCreateImage(cvSize(srcImg->width, srcImg->height), 8, 1);
			cvConvertScale(srcImg, dstImg, 255.0 / (maxVal - minVal), - minVal * 255.0 / (maxVal-minVal));
		}
		return dstImg;
	}

	// Store a greyscale floating-point CvMat image into a BMP/JPG/GIF/PNG image,
	// since cvSaveImage() can only handle 8bit images (not 32bit float images).
	void saveFloatImage(const char *filename, const IplImage *srcImg)
	{
		IplImage *byteImg = convertFloatImageToUcharImage(srcImg);
		cvSaveImage(filename, byteImg);
		cvReleaseImage(&byteImg);
	}

	//Preprocess input image(Convert image to grayscale, detect the face in the image, 
	//crop the face reigen, resize it and then equalize it.)
	//IplImage* preprocessImage(const IplImage* inputImage)
	//{
	//		IplImage *greyImage;
	//		IplImage *faceImage;
	//		IplImage *sizedImage;
	//		IplImage *equalizedImage;
	//		IplImage *processedFaceImage;
	//		CvRect faceRect;

	//		// Make sure the image is greyscale, since the Eigenfaces is only done on greyscale image.
	//		greyImage = convertImageToGreyscale(inputImage);
	//		cvSaveImage("grayscaleImage.bmp", greyImage);
	//		// Load the HaarCascade classifier for face detection.
	//		CvHaarClassifierCascade* faceCascade;
	//		faceCascade = (CvHaarClassifierCascade*)cvLoad(faceCascadeFilename, 0, 0, 0);

	//		// Perform face detection on the input image, using the given Haar cascade classifier.
	//		faceRect = detectFace(greyImage, faceCascade );

	//		// Make sure a valid face was detected.
	//		if (faceRect.width > 0) 
	//		{
	//			faceImage = cropImage(greyImage, faceRect);	// Get the detected face image.
	//			cvSaveImage("croppedImage.bmp", faceImage);
	//			// Make sure the image is the same dimensions as the training images.
	//			sizedImage = resizeImage(faceImage, faceWidth, faceHeight);
	//			cvSaveImage("resizedImage.bmp", sizedImage);
	//			// Give the image a standard brightness and contrast, in case it was too dark or low contrast.
	//			equalizedImage = cvCreateImage(cvGetSize(sizedImage), 8, 1);	// Create an empty greyscale image
	//			cvEqualizeHist(sizedImage, equalizedImage);
	//			cvSaveImage("equalizedImage.bmp", equalizedImage);
	//			processedFaceImage = equalizedImage;
	//			return processedFaceImage;
	//		}

	//		else
	//		{
	//			MessageBox::Show("Could not detect a face in the training image.");
	//			processedFaceImage = inputImg;
	//			return processedFaceImage;
	//		}

	//		cvReleaseImage( &greyImage );
	//		cvReleaseImage( &faceImage );
	//		cvReleaseImage( &sizedImage );
	//		cvReleaseImage( &equalizedImage );
	// }